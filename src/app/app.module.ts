import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/components/header/header.component';
import { JobTitleHeaderComponent } from './common/components/job-title-header/job-title-header.component';
import { FooterComponent } from './common/components/footer/footer.component';
import { JobInfoComponent } from './optimy/job-info/job-info.component';
import { WorkWithMpwrComponent } from './optimy/work-with-mpwr/work-with-mpwr.component';
import { AboutCompanyComponent } from './optimy/about-company/about-company.component';
import { FaqComponent } from './optimy/faq/faq.component';
import { CompanyInfoComponent } from './optimy/company-info/company-info.component';
import { PageNotFoundComponent } from './common/components/page-not-found/page-not-found.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    JobTitleHeaderComponent,
    FooterComponent,
    JobInfoComponent,
    WorkWithMpwrComponent,
    AboutCompanyComponent,
    FaqComponent,
    CompanyInfoComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
