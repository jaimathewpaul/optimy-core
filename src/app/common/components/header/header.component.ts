import {Component, OnInit} from '@angular/core';
import {Language} from 'src/app/core/constants/common.enum';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  language = 'fr';
  isMenuCollapsed = true;

  constructor(private translate: TranslateService) { }

  ngOnInit(): void {
  }

  changeLang() {
    this.language = Language.en === this.language ? Language.fr : Language.en;
    this.translate.use(this.language);
  }

}
