import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkWithMpwrComponent } from './work-with-mpwr.component';

describe('WorkWithMpwrComponent', () => {
  let component: WorkWithMpwrComponent;
  let fixture: ComponentFixture<WorkWithMpwrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkWithMpwrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkWithMpwrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
