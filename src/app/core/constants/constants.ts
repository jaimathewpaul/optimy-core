
export const Routes = {
  empty: '',
  wildCard: '**',
  companyInfo: 'job-site/job',
  jobInfo: 'web-site/job/:id'
};


export const Constants = {
  routes: Routes,
  apiPaths: {
    login: '/api/v1/login/password',
    getTaskList: '/api/v1/task/list',
  },
};

