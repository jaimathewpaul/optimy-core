import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Constants} from "./core/constants/constants";
import {PageNotFoundComponent} from "./common/components/page-not-found/page-not-found.component";
import {CompanyInfoComponent} from "./optimy/company-info/company-info.component";
import {JobInfoComponent} from "./optimy/job-info/job-info.component";


const routes: Routes = [
  {
    path: Constants.routes.companyInfo,
    component: CompanyInfoComponent,
  },
  {
    path: Constants.routes.jobInfo,
    component: JobInfoComponent,
  },
  {
    path: Constants.routes.empty,
    redirectTo: `/${Constants.routes.companyInfo}`,
    pathMatch: 'full'
  },
  {
    path: Constants.routes.wildCard,
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
